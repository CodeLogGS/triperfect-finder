# Triperfect number finder

This is a program to search for triperfect numbers.

## Building

* Firstly, clone the full repository into any directory of your choice.
* Next, pull up your favourite terminal and make sure you have cabal-install installed. On Arch Linux use the command `# pacman -S cabal-install` to install. On Ubuntu / Debian use `# apt-get install cabal-install`
* Next, cd to the directory where the repository is and type `$ cabal configure` to configure the package.
* Finally, use `$ cabal build` and `$ cabal run` to build and run the program.
