triperfect :: Integer -> Bool
triperfect n = sum (getFactors n) == 3 * n

getFactors :: Integer -> [Integer]
getFactors n =
  let half = ceiling(fromInteger n / 2)
  in [x | x <- [1..half], n `mod` x == 0] ++ [n]

main :: IO()
main = do
  putStrLn "Starting search..."
  mapM_ print [x | x <- [1..], triperfect x]
