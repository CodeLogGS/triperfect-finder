# Revision history for triperfect-finder

## 0.0.0.1 -- 2018-06-29

* First version. Released on an unsuspecting world.

## 0.0.0.2 -- 2018-06-30

* Changed the way the range system works, specifically, asking only for a high.

## 0.0.1.0 -- 2018-07-01

* Optimized the getFactors function.
